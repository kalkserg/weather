package org.home.homework;

/**
 * Объект служит для преобразования внешних данных во внутренние данные
 */
public interface DataConverter {
    org.home.homework.InternalData convert(org.home.homework.ProvidedData source);
}
