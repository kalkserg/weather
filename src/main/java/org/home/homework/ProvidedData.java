package org.home.homework;

import java.util.Date;

/**
 * Данные которые мы получаем из внешней системы. Требуют обработки
 */
public class ProvidedData {
    private Date date;
    private float temperature;
    private float wetness;

    public ProvidedData(Date date, float temperature, float wetness) {
        this.date = date;
        this.temperature = temperature;
        this.wetness = wetness;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getWetness() {
        return wetness;
    }

    public void setWetness(float wetness) {
        this.wetness = wetness;
    }
}
